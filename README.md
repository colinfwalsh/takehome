# Current Take Home


First, thanks for the project it was fun making it! I completed all the assigned tasks as well as a couple of additional features.

## Additional features:
- List pagination
    - Infinite scroll so long as there are places within a given radius
- List/Map transition
    - Smoothly transitions between map and list

Unfortunately, I couldn't finish the gradient challenge, but I'll try to take a look at it before now and in a future interview!

## Notes:
- I did not make a secret file for the API keys but noted that in the challenge
- I included my API key since the one provided was invalidated at some point (after review, the key will be invalidated)
- I did my best to mark various sections in the view controllers; I believe I kept things consistent
- I used MVVM to architect the app, so business logic and UI logic should be cleanly separated
- Combine is used to fetch data from the backend, as well as observe publisher changes in various parts of the app

If you have any questions please feel free to email me at colinfwalsh@gmail.com

Thanks again!  Looking forward to any and all feedback.
