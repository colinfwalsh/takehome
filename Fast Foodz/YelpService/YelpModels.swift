//
//  YelpModels.swift
//  YelpModels
//
//  Created by Colin Walsh on 9/9/21.
//

import Foundation

struct YelpData: Decodable {
    let businesses: [YelpBusiness]?
}

struct YelpBusiness: Decodable, Hashable {
    let id: String?
    let alias: String?
    let name: String?
    let imageUrl: String?
    let isClosed: Bool?
    let url: String?
    let reviewCount: Int?
    let categories: [YelpCategory]?
    let coordinates: YelpCoordinate?
    let transactions: [String]?
    let price: String?
    let location: YelpLocation?
    let phone: String?
    let displayPhone: String?
    let distance: Double
    
    var getCategory: String {
        let category = self.categories?
            .compactMap { $0.alias }
            .first(where: { $0 == "chinese" || ($0 == "mexican" || $0 == "tacos") ||
                            $0 == "burgers" || $0 == "pizza"}) ?? ""
        
        if category == "tacos" { return "mexican" }
        if category == "" { return "chinese"}
        
        return category
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: YelpBusiness, rhs: YelpBusiness) -> Bool {
        lhs.id == rhs.id
    }
}

struct YelpCoordinate: Decodable {
    let latitude: Double?
    let longitude: Double?
}

struct YelpLocation: Decodable {
    let address1: String?
    let address2: String?
    let address3: String?
    let city: String?
    let zipCode: String?
    let country: String?
    let state: String?
    let displayAddress: [String]?
}

struct YelpCategory: Decodable {
    let alias: String?
    let title: String?
}
