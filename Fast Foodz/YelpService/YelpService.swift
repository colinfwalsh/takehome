//
//  YelpService.swift
//  YelpService
//
//  Created by Colin Walsh on 9/9/21.
//

import Foundation
import Combine


struct YelpService {
    static let shared = YelpService()
    
    let baseUrl = "https://api.yelp.com/v3/"
    
    enum Categories: String {
        case pizza
        case mexican
        case chinese
        case burgers
    }
    
    enum Endpoint: String {
        case businesses
        case transactions
        case autocomplete
    }
    
    enum Query: String {
        case search
        case reviews
        case phone
    }
    
    enum SortBy: String {
        case bestMatch = "best_match"
        case rating = "rating"
        case reviewCount = "review_count"
        case distance
    }
    
    func buildUrl(with endpoint: Endpoint,
                  queries: [Query],
                  categories: [Categories]?,
                  sortBy: SortBy?,
                  radius: Int? = 1000,
                  limit: Int = 20,
                  offset: Int = 0,
                  coordinates: (Double, Double)) -> URL? {
        
        var url = baseUrl + endpoint.rawValue + "/" + queries.map { $0.rawValue }.joined(separator: "/") + "?"
        
        if let categories = categories {
            url += ("categories='" + categories.map { $0.rawValue }.joined(separator: ",") + "'&")
        }
        
        if let sortBy = sortBy?.rawValue {
            url += ("sort_by=" + sortBy + "&")
        }
        
        if let radius = radius {
            url += "radius=\(radius)&"
        }
        
        // Non-optional values
        let (lat, long) = coordinates
        
        url += "latitude=\(lat)&longitude=\(long)&search_limit=\(limit)&offset=\(offset)"
        
        return URL(string: url)
    }
    
    func buildRequest(with url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        let key = "Bearer " + fallBack
        
        request.addValue(key, forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        return request
    }
    
    func fetchData(with coord: (Double, Double), offset: Int = 0) -> AnyPublisher<YelpData, Error>{
        guard let url = buildUrl(with: .businesses,
                           queries: [.search],
                           categories: [.burgers, .chinese, .mexican, .pizza],
                           sortBy: .distance,
                           radius: 1000,
                           offset: offset,
                           coordinates: coord)
        else { return Fail(error: URLError(.badURL)).eraseToAnyPublisher()}
        
        let request = buildRequest(with: url)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return URLSession
            .shared
            .dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: YelpData.self, decoder: decoder)
            .mapError {
                switch $0 {
                case let decodingError as Swift.DecodingError:
                    switch decodingError {
                    case .typeMismatch(let key, let value):
                        print("error \(key), value \(value) and ERROR: \(decodingError.localizedDescription)")
                    case .valueNotFound(let key, let value):
                        print("error \(key), value \(value) and ERROR: \(decodingError.localizedDescription)")
                    case .keyNotFound(let key, let value):
                        print("error \(key), value \(value) and ERROR: \(decodingError.localizedDescription)")
                    case .dataCorrupted(let key):
                        print("error \(key), and ERROR: \(decodingError.localizedDescription)")
                    default:
                        print("ERROR: \(decodingError.localizedDescription) \n \(decodingError.recoverySuggestion ?? "")")
                    }
                case let urlError as URLError:
                    print("URL Error: " + urlError.localizedDescription)
                default:
                    print("Other error: " + $0.localizedDescription)
                }
                return $0
            }
            .eraseToAnyPublisher()
    }
}
