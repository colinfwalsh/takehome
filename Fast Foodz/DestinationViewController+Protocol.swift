//
//  DestinationViewController+Protocol.swift
//  DestinationViewController+Protocol
//
//  Created by Colin Walsh on 9/13/21.
//

import Foundation
import UIKit

protocol DestinationViewController: UIViewController {
    var viewModel: MainViewModel? { get set }
}
