//
//  MainViewModel.swift
//  MainViewModel
//
//  Created by Colin Walsh on 9/9/21.
//

import Foundation
import Combine

class MainViewModel: ObservableObject {
    @Published
    var items: [YelpBusiness] = []
    
    @Published
    var isLoading = false
    
    @Published
    var didFinishInitialLoad = false
    
    private var cancellables: Set<AnyCancellable> = []
    
    private var runningOffset = 0
    
    private var selectedItem: YelpBusiness? = nil
    
    public func fetchItems(with location: (Double, Double)) {
        guard isLoading == false
        else { return }
        
        runningOffset = self.items.isEmpty ? 0 : runningOffset
        
        isLoading = true
        YelpService
            .shared
            .fetchData(with: location, offset: runningOffset)
            .receive(on: DispatchQueue.main)
            .replaceError(with: YelpData(businesses: []))
            .sink { [weak self] data in
                guard let self = self,
                      let businesses = data.businesses
                else { return }
                
                if !self.didFinishInitialLoad { self.didFinishInitialLoad.toggle() }
                
                self.items.append(contentsOf: businesses)
                self.runningOffset += 20
                self.isLoading = false
            }.store(in: &cancellables)
    }
    
    public func getItemCount() -> Int {
        return items.count
    }
    
    public func setSelected(with item: YelpBusiness?) {
        selectedItem = item
    }
    
    public func setSelected(at index: Int) {
        guard index <= items.count-1,
              index >= 0
        else { return }
        
        selectedItem = items[index]
    }
    
    public func getSelectedBusiness() -> YelpBusiness? {
        return selectedItem
    }
}
