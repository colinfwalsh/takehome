//
//  ViewController.swift
//  Fast Foodz
//

import UIKit
import Combine

class MainViewController: UIViewController {
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var mapContainer: UIView!
    @IBOutlet var listContainer: UIView!
    @IBOutlet var loadingView: UIView!
    
    @Published
    private var selectedIndex = 0
    
    let viewModel: MainViewModel = MainViewModel()
    
    var cancellables: Set<AnyCancellable> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Fast Food Places"
        
        setSegmentBar()
        setupObservers()
        loadSelectedIndex()
    }
}

// MARK: Setup Helpers
extension MainViewController {
    
    func setupObservers() {
        guard cancellables.isEmpty
        else { return }
        
        viewModel
            .$didFinishInitialLoad
            .receive(on: DispatchQueue.main)
            .sink {[weak self] in self?.loadingView.isHidden = $0}
            .store(in: &cancellables)
        
        $selectedIndex
            .receive(on: DispatchQueue.main)
            .sink {[weak self] index in
                guard let self = self,
                      index == self.segmentControl.selectedSegmentIndex
                else { return }
                
                let front: UIView = index == 1 ? self.listContainer : self.mapContainer
                let back: UIView = index == 1 ? self.mapContainer : self.listContainer
                
                self.fadeIn(front: front, toBack: back)
                
                self.fadeOut(view: back) { _ in
                    self.mapContainer.isHidden = index == 1 ? true : false
                    self.listContainer.isHidden = !self.mapContainer.isHidden
                }
                
            }.store(in: &cancellables)
        
        UserLocationManager
            .shared
            .locationPublisher()
            .receive(on: DispatchQueue.main)
            .sink {[weak self] in
                guard let self = self,
                      let lat = $0?.latitude,
                      let long = $0?.longitude
                else { return }
                
                self.viewModel.items.removeAll()
                self.viewModel.fetchItems(with: (lat, long))
            }
            .store(in: &cancellables)
    }
    
    func setSegmentBar() {
        segmentControl.addTarget(self, action: #selector(self.segmentValueChange(_:)), for: .valueChanged)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
    func loadSelectedIndex() {
        guard let selected: Int = UserDataManager.shared.getKey(for: "segmentIndex")
        else { return }
        
        segmentControl.selectedSegmentIndex = selected
        selectedIndex = selected
    }
}

// MARK: Animation Helpers
extension MainViewController {
    func fadeIn(front view: UIView, toBack backView: UIView) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
            view.alpha = 1.0
        }
    }
    
    func fadeOut(view: UIView, completion: @escaping (Bool) -> ()) {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
            view.alpha = 0
        } completion: { completion($0) }
    }
}

// MARK: Actions
extension MainViewController {
    @objc func segmentValueChange(_ sender:UISegmentedControl!) {
        selectedIndex = selectedIndex == 0 ? 1 : 0
        UserDataManager.shared.updateKey(with: "segmentIndex", value: selectedIndex)
    }
}

// MARK: Segue
extension MainViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? DestinationViewController
        else { return }
        
        destination.viewModel = viewModel
    }
}

