//
//  ListViewController.swift
//  Fast Foodz
//
//  Created by Colin Walsh on 9/9/21.
//

import UIKit
import Combine

class ListViewController: UIViewController, DestinationViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var paginationLoadingView: UIActivityIndicatorView!
    
    var dataSource: UITableViewDiffableDataSource<Int, YelpBusiness>?
    
    weak var viewModel: MainViewModel? {
        didSet {
            setupObservers()
        }
    }
    
    var cancellables: Set<AnyCancellable> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setDataSource()
    }
}

// MARK: Setup Helpers
extension ListViewController {
    func setupObservers() {
        guard cancellables.isEmpty
        else { return }
        
        viewModel?
            .$items
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in self?.updateDatasource(with: $0) }
            .store(in: &cancellables)
        
        viewModel?
            .$isLoading
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in self?.paginationLoadingView.isHidden = !$0 }
            .store(in: &cancellables)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.register(UINib(nibName: "FoodCell", bundle: nil),
                           forCellReuseIdentifier: "foodCell")
        tableView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        tableView.separatorStyle = .none
    }
    
    func setDataSource() {
        let providerClosure: (UITableView, IndexPath, YelpBusiness) -> UITableViewCell = {
            guard let cell = $0.dequeueReusableCell(withIdentifier: CellIdentifiers.foodCell.rawValue,
                                                    for: $1) as? FoodCell
            else { return UITableViewCell() }
            
            cell.setCell(with: $2)
            
            return cell
        }
        
        dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: providerClosure)
    }
}

// MARK: Diffable DataSource
extension ListViewController {
    func updateDatasource(with items: [YelpBusiness]) {
        var snapshot = NSDiffableDataSourceSnapshot<Int, YelpBusiness>()
        snapshot.appendSections([0])
        snapshot.appendItems(items)
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}

// MARK: TableView Delegate Methods
extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel?.setSelected(at: indexPath.row)
        performSegue(withIdentifier: SegueIdentifiers.showDetail.rawValue, sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let count = viewModel?.items.count,
              count > 0,
              indexPath.row == count-5
        else { return }
        
        let location = UserLocationManager.shared.getCurrentLocation()
        viewModel?.fetchItems(with: (location.latitude, location.longitude))
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}

// MARK: Segue
extension ListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == SegueIdentifiers.showDetail.rawValue,
              let destination = segue.destination as? FoodDetailViewController
        else { return }
        
        let viewModel = FoodDetailViewModel(model: viewModel?.getSelectedBusiness())
        destination.viewModel = viewModel
    }
}
