//
//  FoodCellTableViewCell.swift
//  Fast Foodz
//
//  Created by Colin Walsh on 9/9/21.
//

import UIKit

class FoodCell: UITableViewCell {
    @IBOutlet var foodIcon: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subheadlineLabel: UILabel!
    @IBOutlet var chevronLabel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        foodIcon.tintColor = .deepIndigo
        titleLabel.textColor = .deepIndigo
        subheadlineLabel.textColor = .lilacGrey
        chevronLabel.tintColor = .deepIndigo
        
    }
    
    func setCell(with item: YelpBusiness) {
        self.titleLabel.text = item.name
        self.subheadlineLabel.attributedText = makeSubheadline(with: item.distance, price: item.price ?? "$")
        self.foodIcon.image = UIImage(named: item.getCategory )
        let bgColorView = UIView()
        bgColorView.backgroundColor = .powderBlue
        self.selectedBackgroundView = bgColorView
        self.layoutIfNeeded()
    }
    
    func makeSubheadline(with distance: Double, price: String) -> NSMutableAttributedString {
        let distance = Measurement(value: distance, unit: UnitLength.meters)
        
        let miles = String(format: "%.2f", distance.converted(to: .miles).value)
        
        let defaultString = "$$$$ • " + miles + " miles"
        
        let range = (defaultString as NSString).range(of: price)

        let mutableAttributedString = NSMutableAttributedString.init(string: defaultString)
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                             value: UIColor.pickleGreen,
                                             range: range)
        
        return mutableAttributedString
    }
}
