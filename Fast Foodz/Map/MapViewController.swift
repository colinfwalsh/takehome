//
//  MapViewController.swift
//  Fast Foodz
//
//  Created by Colin Walsh on 9/9/21.
//

import UIKit
import MapKit
import Combine

class MapViewController: UIViewController, DestinationViewController {
    @IBOutlet var mapView: MKMapView!
    
    private class CustomAnnotation : MKPointAnnotation {
        var identifier: String?
        var index: Int?
    }
    
    var cancellables: Set<AnyCancellable> = []
    
    weak var viewModel: MainViewModel? {
        didSet {
            setupObservers()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapview()
    }
}

// MARK: Setup Helpers
extension MapViewController {
    
    func setupMapview() {
        mapView.delegate = self
        mapView.showsUserLocation = true
    }
    
    func setupObservers() {
        guard cancellables.isEmpty
        else { return }
        
        viewModel?
            .$items
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                guard let self = self
                else { return }
                
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                let annotations = $0.enumerated().compactMap { (index, business) -> MKPointAnnotation? in
                    guard let coord = business.coordinates,
                          let lat = coord.latitude,
                          let long = coord.longitude
                    else { return nil }
                    
                    let annotation = CustomAnnotation()
                    annotation.identifier =  AnnotationIdentifiers.defaultAnnotation.rawValue
                    annotation.index = index
                    annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    
                    return annotation
                }
                
                self.mapView.addAnnotations(annotations)
            }.store(in: &cancellables)
        
        UserLocationManager
            .shared
            .locationPublisher()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                guard let self = self,
                      let center = $0
                else { return }
                
                let coordinateRegion = MKCoordinateRegion(center: center,
                                                          latitudinalMeters: 1000, longitudinalMeters: 1000)
                self.mapView.setRegion(coordinateRegion, animated: true)
            }
            .store(in: &cancellables)
    }
}

// MARK: MKMapView Delegate Methods
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation)
        else { return nil }
        
        let annotationIdentifier = AnnotationIdentifiers.defaultAnnotation.rawValue
        
        guard let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        else {
            let annotationView = MKAnnotationView(annotation: annotation,
                                                  reuseIdentifier: annotationIdentifier)
            
            annotationView.image = UIImage(named: "pin")

            return annotationView
        }
        
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? CustomAnnotation
        else { return }
        
        viewModel?.setSelected(at: (annotation.index) ?? 0)
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
        
        performSegue(withIdentifier: SegueIdentifiers.showDetail.rawValue, sender: self)
    }
}

// MARK: Segue
extension MapViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == SegueIdentifiers.showDetail.rawValue,
              let destination = segue.destination as? FoodDetailViewController
        else { return }
        
        let viewModel = FoodDetailViewModel(model: viewModel?.getSelectedBusiness())
        destination.viewModel = viewModel
    }
}
