//
//  UserLocationManager.swift
//  UserLocationManager
//
//  Created by Colin Walsh on 9/13/21.
//

import Foundation
import CoreLocation
import Combine


/**
 Handles the brunt of setting up location for the user
 
 This is a Singleton because it feeds data to any children, without any risk of the uderlying data being overwritten in multple places
 
 Properties are private, and only getters are public
 */

class UserLocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = UserLocationManager()
    
    private let locationManager : CLLocationManager
    
    @Published
    private var currentLocation: CLLocationCoordinate2D?
    
    private let defaultLocation = CLLocationCoordinate2D(latitude: 40.758896,
                                                         longitude: -73.985130)
    
    override init() {
        locationManager = CLLocationManager()
        
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
    }
    
    public func locationPublisher() -> AnyPublisher<CLLocationCoordinate2D?, Never> {
        return $currentLocation.eraseToAnyPublisher()
    }
    
    public func getCurrentLocation() -> CLLocationCoordinate2D {
        guard let location = currentLocation
        else { return defaultLocation }
                                             
        return location
    }
    
    public func invalidateCurrentLocation() {
        currentLocation = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationCoordinate = locations.first?.coordinate
        else { return }
        
        currentLocation = locationCoordinate
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied, .notDetermined, .restricted:
            currentLocation = defaultLocation
        default:
            locationManager.startUpdatingLocation()
        }
    }
    
}
