//
//  UserDataManager.swift
//  UserDataManager
//
//  Created by Colin Walsh on 9/13/21.
//

import Foundation

/**
 A very simple data manager class, made generic to make it easy to fetch/store values in UserDefaults
 
 This is not the best approach as larger objects will likely be incompatible, but it's a simple implementation that can be expanded
 using CoreData or Realm
 
 Since we're just using this to keep track of the index of the SegmentView, this should server our purposes just fine
 
 */

class UserDataManager: NSObject {
    static let shared = UserDataManager()
    
    func updateKey<T>(with key: String, value: T) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func getKey<T>(for key: String) -> T? {
        guard let fetch = UserDefaults.standard.object(forKey: key),
              let value = fetch as? T
        else { return nil }
        
        return value
    }
}
