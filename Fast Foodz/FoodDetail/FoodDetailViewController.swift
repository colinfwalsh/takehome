//
//  FoodDetailViewController.swift
//  FoodDetailViewController
//
//  Created by Colin Walsh on 9/9/21.
//

import UIKit
import MapKit
import Kingfisher

class FoodDetailViewController: UIViewController {
    
    var viewModel: FoodDetailViewModel?
    
    @IBOutlet var placeImage: UIImageView!
    @IBOutlet var placeName: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var callButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        
        setupNavBar()
        
        guard let viewModel = viewModel
        else { return }
        
        let (destLat, destLong) = viewModel.getDestinationCoordinates()
        getDirections(for: CLLocationCoordinate2D(latitude: destLat,
                                                  longitude: destLong))
        setData()
    }
}

// MARK: Actions
extension FoodDetailViewController {
    @objc func callBusinessTapped(_ sender: Any) {
        guard let url = viewModel?.getTelephoneUrl()
        else { return }
        
        UIApplication.shared.open(url)
    }
    
    @objc func share(sender: UIView) {
        guard let viewModel = viewModel,
              let shareUrl = viewModel.getShareUrl()
        else { return }
        
        let objectsToShare = ["yelp.com", shareUrl] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop,
                                            UIActivity.ActivityType.addToReadingList]
        
        present(activityVC, animated: true, completion: nil)
    }
}

// MARK: Setup Helpers
extension FoodDetailViewController {
    func setupNavBar() {
        
        title = "Details"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"),
                                                            landscapeImagePhone: UIImage(named: "share"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(share(sender:)))
            
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "back",
                                                           style: .plain,
                                                           target: self,
                                                           action: nil)
    }
    
    func setData() {
        self.placeName.text =  viewModel?.getName()
        
        self.callButton.backgroundColor = .competitionPurple
        callButton.addTarget(self, action: #selector(callBusinessTapped(_:)), for: .touchUpInside)
        
        guard let url = viewModel?.getImageUrl()
        else { return }
        
        self.placeImage.kf.setImage(with: url,
                                    options: [.transition(.fade(0.2))])
    }
}

// MARK: MapKit Helpers
extension FoodDetailViewController {
    func getDirections(for destination: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        
        let userLocation = UserLocationManager.shared.getCurrentLocation()
        
        let userPlacemark = MKPlacemark(coordinate: userLocation, addressDictionary: nil)
        
        request.source = MKMapItem(placemark: userPlacemark)
        
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination,
                                                               addressDictionary: nil))
        
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate {[weak self] response, error in
            guard let self = self,
                  let unwrappedResponse = response,
                  let route = unwrappedResponse.routes.first
            else { return }
            
            DispatchQueue.main.async {
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect,
                                               edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50),
                                               animated: true)
            }
            
        }
    }
}


// MARK: MKMapView Delegate Methods
extension FoodDetailViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.lineWidth = 4
        renderer.strokeColor = .bluCepheus
        return renderer
    }
}
