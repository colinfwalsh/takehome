//
//  FoodDetailViewModel.swift
//  FoodDetailViewModel
//
//  Created by Colin Walsh on 9/10/21.
//

import Foundation
import Combine

class FoodDetailViewModel: ObservableObject {
    
    private var model: YelpBusiness?
    
    init(model: YelpBusiness?) {
        self.model = model
    }
    
    func getName() -> String {
        return model?.name ?? ""
    }
    
    func getImageUrl() -> URL? {
        guard let urlString = model?.imageUrl
        else { return nil }
        
        return URL(string: urlString)
    }
    
    func getTelephoneUrl() -> URL? {
        guard let telephone = model?.phone,
              let url = URL(string: "tel://\(telephone)")
        else { return nil }
        
        return url
    }
    
    func getShareUrl() -> URL? {
        guard let urlString = model?.url
        else { return nil }
        
        return URL(string: urlString)
    }
    
    func getDestinationCoordinates() -> (Double, Double) {
        guard let coords = model?.coordinates,
              let lat = coords.latitude,
              let long = coords.longitude
        else { return (0, 0) }
        
        return (lat, long)
    }
}
