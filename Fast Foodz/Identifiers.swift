//
//  Identifiers.swift
//  Identifiers
//
//  Created by Colin Walsh on 9/9/21.
//

import Foundation


/**
 Simple identifiers so we don't have to write out the string every time
 */
enum SegueIdentifiers: String {
    case showDetail
    case embeddedMap
    case embeddedList
}

enum CellIdentifiers: String {
    case foodCell
}

enum AnnotationIdentifiers: String {
    case defaultAnnotation
}
